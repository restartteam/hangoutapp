'use strict';

angular.module('hangoutApp', ['ui.router','ngResource'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html?z'
                    },
                    'messages': {
                        templateUrl : 'views/messages.html?z',
                        controller  : 'IndexController'
                    },
                    'chat': {
                        templateUrl : 'views/chat.html?z',
                        controller  : 'IndexController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html?z'
                    }
                }
            })
            .state('app.chat', {
                url: ':id'
            });
            $urlRouterProvider.otherwise('/');
    })