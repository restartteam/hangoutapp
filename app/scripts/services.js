'use strict';

angular.module('hangoutApp')

        .service('hangoutFactory', ['$resource', function($resource) {

            this.getHangout = function(){
                return $resource("scripts/hangout.json",null);
            };
            

        }])
;