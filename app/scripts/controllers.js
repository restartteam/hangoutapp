'use strict';
angular.module('hangoutApp')

    .controller('IndexController', ['$scope', '$rootScope', '$timeout', 'hangoutFactory', '$state', '$filter', function($scope, $rootScope, $timeout, hangoutFactory, $state, $filter) {

        $scope.chosenMessage = 1;
        if ($state.params.id) {
            $scope.chosenMessage = $state.params.id;
        }
        $scope.showChat = false;
        $scope.getMessages = function() {
            hangoutFactory.getHangout().query(
                function(response) {
                    $scope.messages = response;
                    $scope.curMessage = $filter('filter')($scope.messages, {'id':$scope.chosenMessage});
                    $scope.chat = $scope.curMessage[0].parts;
                    console.log('Чат загружен');
                    $timeout(function() {
                        $scope.showChat = true;
                        console.log('Показываем чат');
                    }, 1000);
                },
                function(response) {
                    console.log("Ошибка: "+response.status + " " + response.statusText);
            });
        }
        $scope.getMessages();

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams){ 
            $scope.showChat = false;
            $scope.chosenMessage = toParams.id;
            $scope.getMessages();
        })

        /* Chat */
        $scope.userChatMessage = {author:"", type:"1", text:""}
        
        $scope.submitChatMessage = function () {
            $scope.chat.push($scope.userChatMessage);
            $scope.chatForm.$setPristine();
            $scope.userChatMessage = {author:"", type:"1", text:""}
        }

    }])
;

